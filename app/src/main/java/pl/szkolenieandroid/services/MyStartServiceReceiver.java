package pl.szkolenieandroid.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import hugo.weaving.DebugLog;

public class MyStartServiceReceiver extends BroadcastReceiver {

    @Override
    @DebugLog
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, CheesesService.class);
        context.startService(service);
    }
}
