package pl.szkolenieandroid.services;

import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import hugo.weaving.DebugLog;


public class MainActivity extends ListActivity {

    private List<String> strings = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, strings);
        setListAdapter(adapter);
    }

    @OnClick(R.id.start)
    @DebugLog
    public void start() {
        AlarmManager service = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(this, MyStartServiceReceiver.class);
        PendingIntent pending = PendingIntent.getBroadcast(this, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        Calendar cal = Calendar.getInstance();
        service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000 * 5, pending);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, CheesesService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(connection);
    }

    private CheesesService cheesesService;
    private ServiceConnection connection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder binder) {
            CheesesService.MyBinder b = (CheesesService.MyBinder) binder;
            cheesesService = b.getService();
            Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
            strings.clear();
            strings.addAll(cheesesService.getList());
            adapter.notifyDataSetChanged();
        }

        public void onServiceDisconnected(ComponentName className) {
            cheesesService = null;
        }
    };

}
